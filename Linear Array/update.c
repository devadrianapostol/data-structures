/*Consider LA is a linear array with N elements and K is a positive integer such that K<=N.
 Below is the algorithm to update an element available at the Kth position of LA.*/
 #include <stdio.h>
 int main(){
    int LA[] = {1,3,5,7,8};
    int k = 3, n = 5, item = 10;
    int i, j;

    printf("The original array elements are :\n");

    for(i = 0; i<n; i++) {
      printf("LA[%d] = %d \n", i, LA[i]);
    }

    LA[k-1] = item;

    printf("The array elements after update: \n");

    for(i=0;i<n;i++){
        printf("LA[%d] = %d \n",i,LA[i]);
    }


    return 0;
 }
